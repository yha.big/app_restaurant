export const tables = [
  {
    id: 1,
    name: "Mesa 1",
    capacity: 4,
  },
  {
    id: 2,
    name: "Mesa 2",
    capacity: 6,
  },
  {
    id: 3,
    name: "Mesa 3",
    capacity: 4,
  },
  {
    id: 4,
    name: "Mesa 4",
    capacity: 8,
  },
  {
    id: 5,
    name: "Mesa 5",
    capacity: 12,
  },
];
