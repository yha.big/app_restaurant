import React, { useState } from "react";
import { View, StyleSheet, ScrollView, Image, Text } from "react-native";
import { Button } from "react-native-paper";
import { useNavigation } from "@react-navigation/native";

import { globalStyle } from "../../styles/general";
import RegisterForm from "../../components/RegisterForm";

const Register = ({ navigation }) => {
  return (
    <ScrollView centerContent={true} style={globalStyle.fondo}>
      <View style={{ marginHorizontal: 40 }}>
        <View style={globalStyle.center}>
          <Image
            source={require("../../../assets/img/logoInicioSesion.png")}
            resizeMode="contain"
            style={styles.logo}
          />

          <Text style={[globalStyle.title, styles.title]}>REGISTRO</Text>
        </View>

        <View style={{ marginBottom: 24 }}>
          <RegisterForm navigation={navigation} />
        </View>
        {/* <CreateAccount navigation={navigation} /> */}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  logo: {
    width: "45%",
    resizeMode: "contain",
  },
  title: {
    fontSize: 24,
    marginBottom: 40,
  },
});

export default Register;
