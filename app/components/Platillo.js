import React, { useState, useEffect, useCallback, useContext } from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  FlatList,
  RefreshControl,
  Dimensions,
  ActivityIndicator,
  SafeAreaView,
  Image,
} from "react-native";
import {
  Text,
  Card,
  Divider,
  Title,
  Paragraph,
  Caption,
} from "react-native-paper";

import { globalStyle } from "../styles/general";
import colors from "../styles/colors";

const { width, height } = Dimensions.get("window");

const Platillo = () => {
  return (
    <View>
      <ScrollView>
        <SafeAreaView style={{ marginHorizontal: 8, flex: 1 }}>
          <Card
            // onPress={() => selectNoticia(item.id)}
            style={{ marginVertical: 8 }}
          >
            <Card.Content>
              <Image
                style={styles.sizeLogo}
                source={require("../../assets/img/coctel-de-cerveza.jpg")}
              />
              <Title numberOfLines={2} style={styles.title}>
                Cóctel de cerveza
              </Title>
              <Caption>Bebída</Caption>
              <Paragraph
                numberOfLines={5}
                style={{
                  color: colors.TEXT,
                  fontFamily: "WorkSans-Regular",
                }}
              >
                El cóctel de cerveza es uno de los más apreciados y fácil de
                preparar que existen. La cerveza puede tomarse de forma
                individual, pero también como base de un cóctel que puede
                resultar especialmente beneficioso.
              </Paragraph>
            </Card.Content>
          </Card>
        </SafeAreaView>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  title: {
    marginTop: 8,
    marginBottom: 0,
    color: colors.BUTTON_ACTIVO,
    fontFamily: "Montserrat-Bold",
    lineHeight: 24,
  },
  containerNoResult: {
    width: "100%",
    paddingVertical: height / 2 - 80,
    textAlignVertical: "center",
    position: "absolute",
  },
  textFooter: {
    marginBottom: 16,
    alignItems: "center",
  },
  sizeLogo: {
    width: "100%",
    height: height / 4,
    alignSelf: "center",
  },
});

export default Platillo;
