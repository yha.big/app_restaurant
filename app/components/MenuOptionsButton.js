import React from "react";
import { Text, View, TouchableOpacity, StyleSheet } from "react-native";

import { globalStyle } from "../styles/general";

const MenuOptionsButton = ({
  title,
  color,
  navigation,
  report,
  department,
  municipality,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      underlayColor={color}
      style={[globalStyle.opcionMenu, { backgroundColor: color }]}
      onPress={() =>
        navigation.navigate(report, {
          department,
          municipality,
        })
      }
    >
      <Text style={globalStyle.labelOpcionMenu}>{title}</Text>
    </TouchableOpacity>
  );
};

export default MenuOptionsButton;
