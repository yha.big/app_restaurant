import React, { useState, useEffect, useContext } from "react";
import { StyleSheet, Text, View } from "react-native";

import { TextInput, Button, List } from "react-native-paper";
import { globalStyle } from "../styles/general";
import colors from "../styles/colors";
import BarButton from "../components/ui/BarButton";
import tableContext from "../context/tables/tableContext";

const PlatillosForm = ({
  setVisibleForm,
  update,
  setUpdate,
  selectedTable,
}) => {
  const [activateButton, setActivateButton] = useState("Título y menú");
  const [expanded, setExpanded] = useState(true);
  const handlePress = () => setExpanded(!expanded);

  const [menuSelect, setMenuSelect] = useState("Seleccione el menú");

  const { tables, TABLECREATE, TABLEUPDATE } = useContext(tableContext);

  const [table, setTable] = useState({ name: "", capacity: 0 });

  useEffect(() => {
    const viewInfo = () => {
      setTable(selectedTable);
    };
    viewInfo();

    console.log("table");
    console.log(table);
  }, [selectedTable]);

  const submit = async () => {
    if (!update) {
      if (table.name.trim() == "" && table.capacity.trim() == "") {
        console.log("Todos los campos son obligatorios");
      } else {
        await TABLECREATE(table);
        console.log("Platillo registrado correctamente");
      }
    } else {
      if (table.name.trim() == "" && table.capacity.trim() == "") {
        console.log("Todos los campos son obligatorios");
      } else {
        await TABLEUPDATE({ data: table, id: table._id });
        console.log("Platillo editado correctamente");
      }
    }
  };

  return (
    <View
      style={{
        backgroundColor: "#fff8df",
        paddingHorizontal: 16,
        marginBottom: 16,
        borderRadius: 8,
      }}
    >
      <Text style={[globalStyle.title, { marginVertical: 8 }]}>
        {!update ? "Nuevo platillo" : "Modificar platillo"}
      </Text>

      <BarButton
        activateButton={activateButton}
        setActivateButton={setActivateButton}
        buttonBackgroundColor={colors.BAR_BUTTON_PLATILLOS}
        titleName={["Título y menú", "Descripción y precio", "Imágen"]}
      />

      {activateButton == "Título y menú" && (
        <View>
          <TextInput
            label="Título"
            placeholder="Título"
            mode="outlined"
            dense={true}
            selectionColor={colors.SELECTION_COLOR}
            value={table.name}
            onChangeText={(text) => setTable({ ...table, name: text })}
          />

          <View
            style={{
              marginVertical: 8,
              backgroundColor: "#eee",
              borderRadius: 5,
              borderWidth: 1,
              borderColor: "#666",
            }}
          >
            <List.Accordion
              title={menuSelect}
              // left={(props) => <List.Icon {...props} icon="folder" />}
              style={{ margin: 0, padding: 0 }}
              expanded={expanded}
              onPress={handlePress}
            >
              <List.Item
                title="Bebída"
                onPress={() => setMenuSelect("Bebída")}
              />
              <List.Item
                title="Desayuno"
                onPress={() => setMenuSelect("Desayuno")}
              />
              <List.Item
                title="Almuerzo"
                onPress={() => setMenuSelect("Almuerzo")}
              />
              <List.Item title="Cena" onPress={() => setMenuSelect("Cena")} />
            </List.Accordion>
          </View>
        </View>
      )}

      {activateButton == "Descripción y precio" && (
        <View>
          <TextInput
            label="Descripción"
            placeholder="Descripción"
            mode="outlined"
            dense={true}
            style={styles.inpuForm}
            selectionColor={colors.SELECTION_COLOR}
            value={table.name}
            onChangeText={(text) => setTable({ ...table, name: text })}
          />
          <TextInput
            label="Precio"
            placeholder="Precio"
            mode="outlined"
            dense={true}
            style={[styles.inpuForm, { marginBottom: 16 }]}
            keyboardType="numeric"
            selectionColor={colors.SELECTION_COLOR}
            value={table.name}
            onChangeText={(text) => setTable({ ...table, name: text })}
          />
        </View>
      )}

      <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
        <Button
          mode="outlined"
          color={colors.BUTTON_ACTIVO}
          onPress={() => {
            setVisibleForm(false);
            setUpdate(false);
          }}
          style={[globalStyle.buttonOutlined, { marginBottom: 16 }]}
          labelStyle={globalStyle.labelButtonOutlined}
        >
          Cancelar
        </Button>

        <Button
          mode="contained"
          color={colors.BUTTON_ACTIVO}
          onPress={() => submit()}
          style={[globalStyle.buttonContent, { marginBottom: 16 }]}
          labelStyle={globalStyle.labelButtonContent}
        >
          {!update ? "Registrar" : "Actualizar"}
        </Button>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  titleCardTable: {
    fontFamily: "Montserrat-Medium",
  },
  inpuForm: {
    marginTop: 10,
  },
});

export default PlatillosForm;
