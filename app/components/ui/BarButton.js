import React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from "react-native";

const BarButton = ({
  activateButton,
  setActivateButton,
  buttonBackgroundColor,
  titleName,
}) => {
  return (
    <View style={{ marginLeft: 8, marginBottom: 4 }}>
      <FlatList
        data={titleName}
        horizontal
        keyExtractor={(titleName, index) => titleName.toString() + (index + 1)}
        renderItem={({ item }) => (
          <TouchableOpacity
            activeOpacity={0.8}
            style={[
              styles.containerButton(buttonBackgroundColor.noSelected),
              activateButton == item &&
                styles.activateColorButton(buttonBackgroundColor.selected),
            ]}
            onPress={() => setActivateButton(item)}
          >
            <Text style={styles.textButton}>{item}</Text>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  containerButton: function (color) {
    return {
      height: 30,
      backgroundColor: color,
      justifyContent: "center",
      marginBottom: 8,
      marginRight: 8,
      borderRadius: 8,
    };
  },
  textButton: {
    color: "#FFF",
    textAlign: "center",
    marginHorizontal: 8,
    fontFamily: "WorkSans-Regular",
  },
  activateColorButton: function (color) {
    return {
      backgroundColor: color,
    };
  },
});
export default BarButton;
