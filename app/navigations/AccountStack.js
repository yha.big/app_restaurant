import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import colors from "../styles/colors";

import Account from "../screens/Account/Account";
import Register from "../screens/Account/Register";
import LogoBar from "../components/LogoBar";

const Stack = createStackNavigator();

const AccountStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="account"
        component={Account}
        options={() => ({
          headerTitle: () => <LogoBar />,
        })}
      />

      <Stack.Screen
        name="register"
        component={Register}
        options={{
          title: "Registro",
          headerTitleStyle: { color: colors.TEXT },
        }}
      />
    </Stack.Navigator>
  );
};

export default AccountStack;
