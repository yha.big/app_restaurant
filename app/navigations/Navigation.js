import React from "react";
import { IconButton } from "react-native-paper";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import colors from "../styles/colors";

import HomepageStack from "../navigations/HomepageStack";
import TableStack from "../navigations/TableStack";
import MenuStack from "../navigations/MenuStack";
import PlatillosStack from "../navigations/PlatillosStack";

const Tab = createBottomTabNavigator();

const Navigation = () => {
  return (
    <>
      <Tab.Navigator
        initialRouteName="homepage"
        tabBarOptions={{
          inactiveTintColor: colors.INACTIVE_TINT,
          activeTintColor: colors.BUTTON_ACTIVO,
        }}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ color }) => screenOptions(route, color),
          headerStyle: {
            backgroundColor: colors.WHITE,
          },
          headerTitleStyle: {
            fontWeight: "100",
          },
        })}
      >
        <Tab.Screen
          name="homepage"
          component={HomepageStack}
          options={() => ({
            title: "Inicio",
          })}
        />

        <Tab.Screen
          name="table"
          component={TableStack}
          options={{ title: "Mesas" }}
        />

        <Tab.Screen
          name="menu"
          component={MenuStack}
          options={{ title: "Menú" }}
        />

        <Tab.Screen
          name="platillos"
          component={PlatillosStack}
          options={{ title: "Platillos" }}
        />
      </Tab.Navigator>
    </>
  );
};

export default Navigation;

function screenOptions(route, color) {
  let iconName;

  switch (route.name) {
    case "homepage":
      iconName = "home-outline";
      break;
    case "table":
      iconName = "table";
      break;
    case "menu":
      iconName = "menu-open";
      break;
    case "platillos":
      iconName = "silverware";
      break;

    default:
      break;
  }

  return <IconButton icon={iconName} color={color} size={22} />;
}
