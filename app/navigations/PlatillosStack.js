import React from "react";
import { Button } from "react-native-paper";
import { FontAwesome5 } from "@expo/vector-icons";
import { createStackNavigator } from "@react-navigation/stack";

import Platillos from "../screens/Platillos";
import Platillo from "../components/Platillo";
import LogoBar from "../components/LogoBar";

const Stack = createStackNavigator();

const PlatillosStack = ({ navigation }) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="platillos"
        component={Platillos}
        options={() => ({
          headerTitle: () => <LogoBar />,
          headerLeft: () => (
            <Button
              onPress={() => navigation.openDrawer()}
              style={{ marginHorizontal: -8 }}
            >
              <FontAwesome5 name="bars" size={24} color="#666" />
            </Button>
          ),
        })}
      />

      <Stack.Screen
        name="platillo"
        component={Platillo}
        options={() => ({
          headerTitle: () => <LogoBar />,
        })}
      />
    </Stack.Navigator>
  );
};

export default PlatillosStack;
