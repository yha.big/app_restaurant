import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { DrawerContent } from "../screens/DrawerContent";

import Navigation from "../navigations/Navigation";
import AccountStack from "../navigations/AccountStack";

const Drawer = createDrawerNavigator();

const NavigationDrawer = () => {
  return (
    <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
      <Drawer.Screen name="home" component={Navigation} />
      <Drawer.Screen name="account" component={AccountStack} />
    </Drawer.Navigator>
  );
};

export default NavigationDrawer;
